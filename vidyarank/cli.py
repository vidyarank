#!/bin/env python
from flask import Flask, render_template, request
import gameart
"""
Vidyarank
-----
A site for making game collages
"""
app = Flask(__name__, template_folder = 'templates/')
app.config.from_object(__name__)
@app.route('/')
def my_form():
    return render_template("index.html")

@app.route('/', methods=['POST'])
def my_form_post():

    text = request.form['text']
    processed_text = gameart.get(text)
    return '<meta http-equiv="refresh" content="0; url={}" />'.format(processed_text)

if __name__ == '__main__':
    app.run(debug=True)

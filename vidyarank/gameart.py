#!/bin/env python
from urllib import request,parse
import xml.etree.ElementTree as ET
import re
"""
Gets a gameart for selected game
"""

def main():
    """Main function"""
    print(get("heroes of might and magic iii"))

def exact_name(name):
    """Changes name of the game so it matches the database"""
    name_url = "http://thegamesdb.net/api/GetGame.php?name={}".format(name)
    data = openurl(name_url)
    xmldoc = ET.parse(data)
    try:
        game = xmldoc.findall('Game')[0]
    except IndexError:
        return 'Wrong game'
        exit()
    title = game.find('GameTitle')
    return title.text

def openurl(url):
    """Opens url"""
    req = request.Request(
        url,
        data=None,
        headers={
            'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_3) ' \
            ' AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.1916.47 Safari/537.36'
            }
        )
    return request.urlopen(req)

def get(title):
    """Opens freaking XML"""
    while True:
        try:
            game_id = "http://thegamesdb.net/api/GetGame.php?exactname={}".format(parse.quote(title))
            data = openurl(game_id)
            xmldoc = ET.parse(data)
            url = xmldoc.find('baseImgUrl').text
            game = xmldoc.findall('Game')[0]
            id_game = game.find('Images')
            boxart = id_game.findall('boxart')
            break
        except IndexError:
            title = exact_name(title)
    for i in boxart:
        if i.get('side') == 'front':
            url += i.text
            break
    return url
    #It could have been so simple with JSON...
if __name__ == '__main__':
    main()

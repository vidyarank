#!/bin/env python
"""
Vidyarank
----
Vidyarank is a site for making collages
"""
from setuptools import setup


setup(
    name='limf',
    version='0.0.0',
    url='http://repo.or.cz/vidyarank.git',
    license='MIT',
    author='Mikołaj Halber',
    author_email='lich@openmailbox.com',
    description='Vidyarank is a site for making collages',
    long_description='Just read README.md. I will create .rst file when I want to do it.',
    packages=['vidyarank'],
    install_requires=[
        'Flask>=0.9',
    ],
    classifiers=[
        'Programming Language :: Python',
        'Programming Language :: Python :: 3',
    ],
    entry_points={
        'console_scripts': ['vidyarank=vidyarank.cli:main']}
)



